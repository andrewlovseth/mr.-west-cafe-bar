<?php if(get_field('show_announcement', 'options') == 'show'): ?>

	<section id="announcement">
		<div class="wrapper">
		
			<div class="info">

				<?php if(get_field('link', 'options')): ?>
					<h2>
						<a href="<?php the_field('link', 'options'); ?>">
							<?php the_field('copy', 'options'); ?>
						</a>
					</h2>

				<?php else: ?>

					<h2><?php the_field('copy', 'options'); ?></h2>

				<?php endif; ?>

			</div>	

		</div>
	</section>


<?php endif; ?>