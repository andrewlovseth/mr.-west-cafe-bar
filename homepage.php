<?php

/*

	Template Name: Home

*/

get_header(); ?>
	

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>)">
	
		<div class="wrapper">

			<img src="<?php $image = get_field('hero_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

		</div>	
	</section>


	<section id="info">
		<div class="wrapper">

			<h3>
				<?php the_field('info_headline'); ?><br/>
				<span><?php the_field('info_sub_headline'); ?></span>
			</h3>

			<section class="hours">

				<h4>
					<?php the_field('hours'); ?><br/>
					<?php the_field('happy_hour_hours'); ?>
				</h4>

			</section>
			
			<section class="events">
				
				<a href="<?php the_field('events_link'); ?>" rel="external">
					<img src="<?php $image = get_field('events_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</section>
			
			
			<section class="gift-cards">
				<a href="<?php the_field('gift_cards_link'); ?>"><span><?php the_field('gift_cards_link_label'); ?></span></a>
			</section>

		</div>	
	</section>


	<section id="gallery">
		<div class="wrapper">


			<?php $images = get_field('gallery'); if( $images ): ?>
				<?php foreach( $images as $image ): ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php endforeach; ?>
			<?php endif; ?>


		</div>	
	</section>


	<section id="menu" class="cover">
		<div class="wrapper">

			<h3>
				<span class="year">20</span>
				<span class="edition">
					<em>Edition</em>
					<strong><?php the_field('menu_season'); ?></strong>
				</span>
				<span class="year"><?php the_field('menu_year'); ?></span>
			</h3>

		</div>	
	</section>


	<section class="menu">
		<div class="wrapper">


			<?php if(get_field('happy_hour_mobile')): ?>
	
				<div class="mobile">
					<img src="<?php $image = get_field('happy_hour_mobile'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				
			<?php endif; ?>
			

			<?php if(get_field('happy_hour')): ?>

				<div class="desktop">
					<img src="<?php $image = get_field('happy_hour'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			<?php endif; ?>

		</div>	
	</section>
	

	<section class="menu">
		<div class="wrapper">

			<?php if(get_field('noon_mobile')): ?>

				<div class="mobile">
					<img src="<?php $image = get_field('noon_mobile'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			<?php endif; ?>
			

			<?php if(get_field('noon')): ?>

				<div class="desktop">
					<img src="<?php $image = get_field('noon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			<?php endif; ?>

		</div>	
	</section>
	
	
	<section class="menu">
		<div class="wrapper">


			<?php if(get_field('night_mobile')): ?>

				<div class="mobile">
					<img src="<?php $image = get_field('night_mobile'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			<?php endif; ?>


			<?php if(get_field('night')): ?>

				<div class="desktop">
					<img src="<?php $image = get_field('night'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			<?php endif; ?>

		</div>	
	</section>
	
		
	<section class="menu">
		<div class="wrapper">


			<?php if(get_field('morning_mobile')): ?>
	
				<div class="mobile">
					<img src="<?php $image = get_field('morning_mobile'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				
			<?php endif; ?>
			

			<?php if(get_field('morning')): ?>

				<div class="desktop">
					<img src="<?php $image = get_field('morning'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			<?php endif; ?>

		</div>	
	</section>


	<section class="menu">
		<div class="wrapper">

			<?php if(get_field('drinks_mobile')): ?>

				<div class="mobile">
					<img src="<?php $image = get_field('drinks_mobile'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				
			<?php endif; ?>
			

			<?php if(get_field('drinks')): ?>
			
				<div class="desktop">
					<img src="<?php $image = get_field('drinks'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			<?php endif; ?>

		</div>	
	</section>


	
<?php get_footer(); ?>