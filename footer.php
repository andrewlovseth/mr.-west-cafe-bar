	
		<footer id="contact" class="cover">
			<div class="wrapper">

				<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>

				<p>
					<?php the_field('street_address_1'); ?> | <?php the_field('street_address_2'); ?> | <a href="<?php the_field('google_maps_link'); ?>">Map us here</a>
				</p>

				<p><?php the_field('phone'); ?></p>

				<section id="social">
					<a href="<?php the_field('facebook'); ?>" class="ir facebook" rel="external">Facebook</a>
					<a href="<?php the_field('twitter'); ?>" class="ir twitter" rel="external">Twitter</a>
					<a href="<?php the_field('instagram'); ?>" class="ir instagram" rel="external">Instagram</a>
				</section>

			</div>
		</footer>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>