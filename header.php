<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<?php get_template_part('partials/announcement'); ?>
	
	<header>
		<div class="header-wrapper">
			
			<a id="hamburger" href="#">
				<div class="patty"></div>
			</a>

			<nav>
				<ul>
					<li><a href="#hero">Home</a></li>
					<li><a href="#info">Gallery</a></li>
					<li><a href="#menu">Menu</a></li>
					<li><a href="#contact">Contact</a></li>
				</ul>
			</nav>

		</div>
	</header>