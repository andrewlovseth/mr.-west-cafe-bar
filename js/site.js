$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});	


    $('#hamburger').click(function(){
        $('body').toggleClass('nav-open');
        return false;
    }); 


    $('nav a').smoothScroll();   

    var announcementHeight = $('section#announcement').outerHeight();
    $('header').css({ top: announcementHeight });
});


$(window).on('resize', function(){
    var announcementHeight = $('section#announcement').outerHeight();
    $('header').css({ top: announcementHeight });
});


if($('section#announcement').length) {
 	var topofDiv = $('section#announcement').offset().top;
	var height = $('section#announcement').outerHeight();
 } else {
	var topofDiv = 0;
	var height = 0;
}

$(window).scroll(function(){
    if($(window).scrollTop() > (topofDiv + height)){
       $('header').addClass('sticky');
    }
    else{
       $('header').removeClass('sticky');
    }
});

